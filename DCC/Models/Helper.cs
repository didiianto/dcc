﻿using DCC.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCC.Models
{
    public class Helper
    {
        public static List<UserViewModel> GetUser()
        {
            var userList = new List<UserViewModel>();
            userList.Add(new UserViewModel { Loginid = "Atasan", Password = "1234", Role = 1 });
            userList.Add(new UserViewModel { Loginid = "Departemen", Password = "1234", Role = 2 });
            userList.Add(new UserViewModel { Loginid = "DCC", Password = "1234", Role = 3 });
            userList.Add(new UserViewModel { Loginid = "QA", Password = "1234", Role = 4 });
            userList.Add(new UserViewModel { Loginid = "Inisiator", Password = "1234", Role = 10 });

            return userList;
        }
        static string[,] _ddlValueDesc = new string[,]
        {
            { "01", "Approve"},
            { "02", "Reject"},
        };
        static string[,] _ddlValuePabrik = new string[,]
        {
            { "01", "DVL Gunung Putri"},
            { "02", "DVL Citereup"},
        };
        static string[,] _ddlValueDep = new string[,]
        {
            { "01", "GP - Plant Manager"},
            { "02", "GP - Production"},
            { "03", "GP - QA"},
            { "04", "GP - R&D"},
            { "05", "CT - Plant Manager"},
            { "06", "CT - Production"},
            { "07", "CT - QA"},
            { "08", "CT - R&D"},
        };
        static string[,] _ddlValueStatus = new string[,]
        {
            { "1", "Sudah disetujui"},
            { "2", "Ditolak"},
            { "3", "Proses Revisi"},
        };
        public static string GetValueDdl(string val)
        {
            string result = "";

            for (int x = 0; x < _ddlValueDesc.GetLength(0); x++)
            {
                if (_ddlValueDesc[x, 0] == val)
                {
                    result = _ddlValueDesc[x, 1];
                    break;
                }
            }
            return result;
        }
        public static string GetValueDdlStatus(string val)
        {
            string result = "";

            for (int x = 0; x < _ddlValueStatus.GetLength(0); x++)
            {
                if (_ddlValueStatus[x, 0] == val)
                {
                    result = _ddlValueStatus[x, 1];
                    break;
                }
            }
            return result;
        }
        public static string GetValueDdlPabrik(string val)
        {
            string result = "";

            for (int x = 0; x < _ddlValuePabrik.GetLength(0); x++)
            {
                if (_ddlValuePabrik[x, 0] == val)
                {
                    result = _ddlValuePabrik[x, 1];
                    break;
                }
            }
            return result;
        }

        public static string GetValueDdlDep(string val)
        {
            string result = "";
            string[] itemList = val.Split(new string[] { "|" }, StringSplitOptions.None);

            foreach(var item in itemList)
            {
                for (int x = 0; x < _ddlValueDep.GetLength(0); x++)
                {
                    if (_ddlValueDep[x, 0] == item)
                    {
                        result += _ddlValueDep[x, 1] + "| ";
                    }
                }
            }
            

            return result;
        }

        public static List<DepartmentTerkait> GetDepartemenTerkait()
        {
            var dep = new List<DepartmentTerkait>();
            dep.Add(new DepartmentTerkait { ValueDep = "01", DescDep = "GP - Plant Manager" });
            dep.Add(new DepartmentTerkait { ValueDep = "02", DescDep = "GP - Production" });
            dep.Add(new DepartmentTerkait { ValueDep = "03", DescDep = "GP - QA" });
            dep.Add(new DepartmentTerkait { ValueDep = "04", DescDep = "GP - R&D" });
            dep.Add(new DepartmentTerkait { ValueDep = "05", DescDep = "CT - Plant Manager" });
            dep.Add(new DepartmentTerkait { ValueDep = "06", DescDep = "CT - Production" });
            dep.Add(new DepartmentTerkait { ValueDep = "07", DescDep = "CT - QA" });
            dep.Add(new DepartmentTerkait { ValueDep = "08", DescDep = "CT - R&D" });

            return dep;
        }

        public static List<string> GetValueCheckbox(string val)
        {
            List<string> result = new List<string>();   
            string[] itemList = val.Split(new string[] { "|" }, StringSplitOptions.None);
            foreach (string item in itemList)
            {
                result.Add(item);   
            }

            return result;
        }

        public static class Status
        {
            public const string Approved = "01";
            public const string Rejected = "02";
            public const string Revised = "03";
        }
    }
}