﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DCC.Models
{
    public class DocumentProgress
    {
        public Guid Id { get; set; }
        public Nullable<DateTime> DateCreated { get; set; }
        public string DocumentNo { get; set; }
        public string Inisiator { get; set; }
        public string Pabrik { get; set; }
        public string Produk { get; set; }
        public string Deskripsi { get; set; }
        public string Dep_Terkait { get; set; }
        public string App_Atasan { get; set; }
        public string App_Department { get; set; }
        public string App_DCC { get; set; }
        public string App_QA { get; set; }
        public Nullable<DateTime> Date_App_Atasan { get; set; }
        public Nullable<DateTime> Date_App_Department { get; set; }
        public Nullable<DateTime> Date_App_DC { get; set; }
        public Nullable<DateTime> Date_App_QA { get; set; }
        public Nullable<int> IsApproved { get; set; }
        public Nullable<DateTime> DateModified { get; set; }
    }


}