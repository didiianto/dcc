﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using DCC.Models.DB;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DCC.Models.DB
{
    public class AppDBContext : DbContext
    {
        public AppDBContext() : base("AppDBContext")
        {
        }

        public DbSet<DocumentProgress> DocumentProgress { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}