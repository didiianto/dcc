﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DCC.Models.ViewModel
{
    public class DCViewModel
    {
        public DocumentProgress DocProgress { get; set; }
        public List<DepartmentTerkait> DepartmentTerkait { get; set; }

    }
    public class DepartmentTerkait
    {
        public string ValueDep { get; set; }
        public string DescDep { get; set; }
        public bool IsChecked { get; set; }
    }

}



public enum PabrikOpt
{
    DVLGunungSindur,
    DVLCitereup
}

