﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCC.Models.ViewModel
{
    public class UserViewModel
    {
            public string Loginid { get; set; }
            public string Password { get; set; }
            public int Role { get; set; }
    }
}