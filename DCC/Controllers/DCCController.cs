﻿using DCC.Models;
using DCC.Models.DB;
using DCC.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using static DCC.Models.ViewModel.DCViewModel;
using Rotativa;


namespace DCC.Controllers
{
    public class DCCController : Controller
    {
        // GET: DCC
        private AppDBContext db = new AppDBContext();

        // GET: Launch
        public ActionResult Index(string sortOrder)
        {
            if(Session["user"] == null)
                return RedirectToAction("Login");

            ViewBag.Status = String.IsNullOrEmpty(sortOrder) ? "status_desc" : "";

            var doc = from s in db.DocumentProgress
                      select s;
            switch (sortOrder)
            {
                case "status_desc":
                    doc = doc.OrderByDescending(s => s.IsApproved);
                    break;
            }

            return View(doc.ToList());
        }

        // GET: Launch/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentProgress documentProgress = db.DocumentProgress.Find(id);
            if (documentProgress == null)
            {
                return HttpNotFound();
            }
            return View(documentProgress);
        }

        //// GET: Launch/Create
        public ActionResult Create()
        {
            var model = new DCViewModel();
            model.DepartmentTerkait = GetDepartemenTerkait();
            return View(model);
        }
        public List<DepartmentTerkait> GetDepartemenTerkait()
        {
            var dep = new List<DepartmentTerkait>();
            dep.Add(new DepartmentTerkait{ ValueDep = "01", DescDep = "GP - Plant Manager" });
            dep.Add(new DepartmentTerkait { ValueDep = "02", DescDep = "GP - Production" });
            dep.Add(new DepartmentTerkait { ValueDep = "03", DescDep = "GP - QA" });
            dep.Add(new DepartmentTerkait { ValueDep = "04", DescDep = "GP - R&D" });
            dep.Add(new DepartmentTerkait { ValueDep = "05", DescDep = "CT - Plant Manager" });
            dep.Add(new DepartmentTerkait { ValueDep = "06", DescDep = "CT - Production" });
            dep.Add(new DepartmentTerkait { ValueDep = "07", DescDep = "CT - QA" });
            dep.Add(new DepartmentTerkait { ValueDep = "08", DescDep = "CT - R&D" });

            return dep;
        }
        public ActionResult Login()
        {
            return View("Login");
        }
        public ActionResult PrintPartialViewToPdf(Guid id)
        {
            DocumentProgress documentProgress = db.DocumentProgress.Find(id);
            DCViewModel model = new DCViewModel();
            model.DocProgress = documentProgress;
            var report = new PartialViewAsPdf("~/Views/DCC/Details.cshtml", model);
            return report;
        }

        public ActionResult LoginProces(UserViewModel user)
        {
            var userList = Helper.GetUser();
            var found = userList.Where(x => x.Loginid == user.Loginid && x.Password == user.Password).ToList();
            if (found.Count == 1)
            {
                Session["user"] = found[0];
                return RedirectToAction("Index");
            }

            return View("Login");
        }

        // POST: Launch/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DCViewModel model)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in model.DepartmentTerkait)
            {
                if (item.IsChecked)
                {
                    sb.Append(item.ValueDep + "|");
                }
            }

            var document = new DocumentProgress();
            document.Id = Guid.NewGuid();
            document.DateCreated = DateTime.Now;
            document.DocumentNo = model.DocProgress.DocumentNo;
            document.Inisiator = model.DocProgress.Inisiator;
            document.Pabrik = model.DocProgress.Pabrik;
            document.Produk = model.DocProgress.Produk;
            document.Deskripsi = model.DocProgress.Deskripsi;

            document.Dep_Terkait = sb.ToString();

            if (ModelState.IsValid)
            {
                db.DocumentProgress.Add(document);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DCViewModel model)
        {
            var user = (UserViewModel)Session["user"];

            DocumentProgress documentProgress = db.DocumentProgress.Find(model.DocProgress.Id);
            if (user.Role == 1)
            {
                documentProgress.App_Atasan = model.DocProgress.App_Atasan;
                documentProgress.Date_App_Atasan = DateTime.Now;
            }
            else if (user.Role == 2)
            {
                documentProgress.App_Department = model.DocProgress.App_Department;
                documentProgress.Date_App_Department = DateTime.Now;
            }
            else if (user.Role == 3)
            {
                documentProgress.App_DCC = model.DocProgress.App_DCC;
                documentProgress.Date_App_DC = DateTime.Now;
            }
            else if (user.Role == 4)
            {
                documentProgress.App_QA = model.DocProgress.App_QA;
                documentProgress.Date_App_QA = DateTime.Now;
            }
             

            if (documentProgress.App_Atasan == Helper.Status.Approved && documentProgress.App_Department == Helper.Status.Approved && documentProgress.App_DCC == Helper.Status.Approved && documentProgress.App_QA == Helper.Status.Approved)
                documentProgress.IsApproved = 1;

            if (documentProgress.App_Atasan == Helper.Status.Rejected || documentProgress.App_Department == Helper.Status.Rejected || documentProgress.App_DCC == Helper.Status.Rejected || documentProgress.App_QA == Helper.Status.Rejected)
                documentProgress.IsApproved = 2;

            if (user.Role == 10)
            {
                documentProgress.Produk = model.DocProgress.Produk;
                documentProgress.Deskripsi = model.DocProgress.Deskripsi;
                documentProgress.IsApproved = 3;
                StringBuilder sb = new StringBuilder();
                foreach (var item in model.DepartmentTerkait)
                {
                    if (item.IsChecked)
                    {
                        sb.Append(item.ValueDep + "|");
                    }
                }
                documentProgress.Dep_Terkait = sb.ToString();
            }

            documentProgress.DateModified = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(documentProgress).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(documentProgress);
        }
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var model = new DCViewModel();
            DocumentProgress documentProgress = db.DocumentProgress.Find(id);
            model.DocProgress = documentProgress;
            model.DepartmentTerkait = GetDepartemenTerkait();
            var checkBox = Helper.GetValueCheckbox(documentProgress.Dep_Terkait);
            foreach (var item in model.DepartmentTerkait.Where(a => checkBox.Contains(a.ValueDep)))
            {
                item.IsChecked = true;  
            }
            if (documentProgress == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}