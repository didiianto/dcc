﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DCC.Startup))]
namespace DCC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
